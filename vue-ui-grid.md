> **ok** 表示已经支持  
> **ns** 表示不支持  
> **auto** 表示element-ui table已经本身就支持，只需要做简单适配

1. 编写一套表格组件，并且兼容ui-grid的配置项。
    1. 其实是支持grid/index.js这个组件所暴露的api，所有需要支持模块在grid/index.js中可以看到。
        1. options          `ok`
        2. watermark
        3. pagination       `ok`
        4. saveGridState    `ns`
        5. selection        `ok`
        6. pinning          `auto`
        7. tree-view        `ns`      collectRecord中用到，这个页面没有用了，无需支持
        8. grouping
        9. autoResize       `ns`
        10. loading         `ok`
        11. loadingText     `ok`
    2. 需要支持的options配置项，且具备响应式特性（看代码穷举）gridOptions.及grid.options.
        1. core
            1. appScopeProvider                 `ok`
            1. columnDefs                       `ok`
            1. data                             `ok`
            1. enableColumnMenus
            1. enableGridMenu
            1. enableSorting
            1. minRowsToShow                    `ns` 不好实现
            1. useExternalSorting
            1. virtualizationThreshold          `ns`
            1. onRegisterApi                    `ok`
        1. selection
            1. enableRowHeaderSelection         `ok`
            1. multiSelect                      `ok`
            1. noUnselect                       `ok`
        1. pagination
            1. totalItems                       `ok`
            1. paginationCurrentPage            `ok`
            1. paginationPageSizes
            1. paginationPageSize               `ok`
            1. useExternalPagination            `ok`
        1. loadData不用关心，外部使用
        1. render 这是自行扩展的 bike/release/report/index.js中用到
        1. isLoading 无需支持，只是刚好挂载在option上的一个属性
        1. enableExpandAll  expandable插件中才会用到，系统中没有用到 layout/index.html 