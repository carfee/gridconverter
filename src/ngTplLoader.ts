import { compileHtmlFile, convertTemplate } from './index';
export default function (source) {
  return compileHtmlFile(source);
};
