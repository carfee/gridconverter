import * as glob from 'glob';
import * as fse from 'fs-extra';
import * as htmlparser2 from 'htmlparser2';
import * as compiler from 'vue-template-compiler';
import AttributeConverter from './attrs';
import { resolve } from 'path';

const htmlFiles = glob.sync('**/*.html', {
  cwd: '../easybikeAdminWeb/src',
});

/**
 * 将ngTemplate编译成vue组件的配置对象
 * @param html ng-template
 * @returns Vue组件的配置对象的js代码
 */
export function convertTemplate(html: string): String {
  html = html.replace(/\{\{/g, '<![CDATA[{{').replace(/\}\}/g, '}}]]>');
  const root = {
    children: [],
  };
  const elStack = [root];

  const parser = new htmlparser2.Parser({
    onopentag(tag, attrs) {
      const currentEl = elStack[elStack.length - 1];

      Object.keys(attrs).forEach(name => {
        attrs[name] = attrs[name].replace(/\<\!\[CDATA\[\{\{/g, '{{').replace(/\}\}\]\]\>/g, '}}');
      });
      const newEl = {
        tag,
        children: [],
        attrs: AttributeConverter.handleAllAttrs(attrs),
      };

      currentEl.children.push(newEl);
      elStack.push(newEl);
    },
    ontext(text: string) {
      console.log(text);
      const currentEl = elStack[elStack.length - 1];
      currentEl.children.push(text.replace(/::/g, ''));
    },
    onclosetag(tag) {
      elStack.pop();
    },
  }, {
    recognizeCDATA: true,
  });

  parser.write(html);
  parser.end();

  const toHtml = (elObj: any) => {
    if (typeof elObj === 'string') {
      return elObj;
    }
    return `<${elObj.tag} ${Object.keys(elObj.attrs).map(name => `${name}="${elObj.attrs[name]}"`).join(' ')}>${
      elObj.children.map(toHtml).join('')
    }</${elObj.tag}>`;
  };

  return `{
  props: {
    grid: {
      type: Object,
      required: true,
    },
    row: {
      type: Object,
      required: true,
    },
  },
  render: function () {
    ${compiler.compile(toHtml(root.children.find(el => typeof el !== 'string'))).render}
  },
}`;
}

export const excludeTplIds = {
  'menuTpl': true,
  'ui-grid/uiGridMenu': true,
  'ui-grid/expandableTopRowHeader': true,
  'statisticalIndicesPopoverTemplate.html': true,
}
/**
 * 从整个Html中转换ngTemplate
 * @param html html文件
 * @returns 编译后的html内容
 */
export function compileHtmlFile(html: string): string {
  const compileResult = [];
  let templateId;
  let matched = false;
  let template = [];
  const parser = new htmlparser2.Parser({
    onopentag(tag, attrs) {
      if (tag === 'script' && attrs.type === 'text/ng-template' && !excludeTplIds[attrs.id]) {
        matched = true;
        templateId = attrs.id;
        template = [];
        compileResult.push('<script>')
      } else {
        compileResult.push(`<${tag} ${Object.keys(attrs).map(name => `${name}="${attrs[name]}"`).join(' ')}>`)
      }
    },
    ontext(text) {
      if (matched) {
        template.push(text);
      } else {
        compileResult.push(text);
      }
    },
    onclosetag(tag) {
      if (tag === 'script' && matched) {
        const ngTpl = template.join('');
        compileResult.push(`window.CELL_TPLS = window.CELL_TPLS || {}; window.CELL_TPLS['${templateId}'] = ${convertTemplate(ngTpl)}`);
        compileResult.push('</script>');
      } else {
        compileResult.push(`</${tag}>`);
      }
    },
  }, {
    recognizeCDATA: true,
  });
  parser.write(html);
  parser.end();
  return compileResult.join('');
}

if (resolve(__dirname, __filename) === process.argv[1]) {
  console.log(compileHtmlFile(process.argv[2]));
}
