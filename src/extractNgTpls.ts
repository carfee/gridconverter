import * as glob from 'glob';
import * as fse from 'fs-extra';
import * as htmlparser2 from 'htmlparser2';
import { resolve } from 'path';

import { excludeTplIds } from '.';

/**
 * 从整个Html中转换ngTemplate
 * @param html html文件
 * @returns 以模板id为键的转换后template
 */
export function extractNgTpls(html: string): { [key: string]: string } {
  const compileResult = [];
  const ngTemplateScripts = {};
  let templateId;
  let matched = false;
  let template = [];
  const parser = new htmlparser2.Parser({
    onopentag(tag, attrs) {
      if (tag === 'script' && attrs.type === 'text/ng-template' && !excludeTplIds[attrs.id]) {
        matched = true;
        templateId = attrs.id;
        template = [];
        compileResult.push('<script>')
      } else {
        compileResult.push(`<${tag} ${Object.keys(attrs).map(name => `${name}="${attrs[name]}"`).join(' ')}>`)
      }
    },
    ontext(text) {
      if (matched) {
        template.push(text);
      }
    },
    onclosetag(tag) {
      if (tag === 'script' && matched) {
        ngTemplateScripts[templateId] = template.join('');
      }
    },
  });

  parser.write(html);
  parser.end();
  return ngTemplateScripts;
}

export function getAllNgTemplates(targetDir) {
  const htmlFiles = glob.sync('**/*.html', {
    cwd: targetDir,
  });
  const result = {};
  htmlFiles.forEach(htmlFilePath => {
    const htmlFileAbsPath = resolve(targetDir, htmlFilePath);
    const htmlFileContent = fse.readFileSync(htmlFileAbsPath, { encoding: 'utf-8' });
    const tpls = extractNgTpls(htmlFileContent);
    result[htmlFilePath] = tpls;
  });
  return result;
}

if (resolve(__dirname, __filename) === process.argv[1]) {
  const dir = process.argv[2];
  if (dir) {
    const allTpls = getAllNgTemplates(dir);
    let fileCount = 0;
    let total = 0;
    Object.keys(allTpls).forEach(htmlFilePath => {
      const tpls = allTpls[htmlFilePath];
      if (Object.keys(tpls).length) {
        console.log(`${++fileCount} ===============${htmlFilePath}`);
        let tplCount = 0;
        Object.keys(tpls).forEach(tplId => {
          if (tpls[tplId].indexOf('ui-grid') === -1) {
            console.error('可疑', tplId, tpls[tplId]);
          }
          console.log(`${++tplCount} >>>>>>>>>>>>>>>${tplId}`);
          console.log(tpls[tplId]);
          total += 1;
        });
      }
    });
    console.log(`共计${total}个模板`);
  }
}
