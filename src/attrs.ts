function camelize(name: string) {
  if (name === 'class') {
    return 'className';
  } else if (name === 'name') {
    return 'domName';
  }
  return name.replace(/-(\w)/g, (m1, m2: string) => m2.toUpperCase());
}

export function parseAttrValue(attrName: string, expr: string) {
  if (expr.indexOf('{{') === -1) {
    return  {
      name: attrName,
      value: expr,
    };
  }
  const compiledResult = [];
  const exprRegx = /\{\{(.+?)\}\}/g;
  let result = exprRegx.exec(expr);
  let lastIndex = 0;
  while(result !== null) {
    let [match, subExpr] = result;
    const { index } = result;
    if (subExpr.indexOf('::') === 0) {
      subExpr = subExpr.substr(2);
    }
    compiledResult.push(`'${expr.substring(lastIndex, index)}' + ${subExpr}`);
    lastIndex = exprRegx.lastIndex;

    result = exprRegx.exec(expr);
  }
  return {
    name: `:${attrName}`,
    value: compiledResult.join(' + '),
  };
}

export default class AttributeConverter {
  public static handleAllAttrs(attrs: Object): Object {
    const result = {};
    Object.keys(attrs).forEach((attrName) => {
      const { name, value } = this.handle(attrName, attrs[attrName]);
      result[name] = value;
    });
    return result;
  }
  /**
   * 将ngTemplate的属性转化为Vue createElement的data对象
   * @param attrName 属性名
   * @param attrValue 属性值
   */
  public static handle(attrName, attrValue): { name: string, value: string } {
    const camelizeName = camelize(attrName);
    if (this[camelizeName]) {
      if (typeof this[camelizeName] === 'function') {
        return this[camelizeName](attrName, attrValue);
      }
      console.log('xxx', camelizeName);
    }
    return this.attrDefaultHandler(attrName, attrValue);
  }

  private static attrDefaultHandler(attrName: string, attrVal: string): { name: string, value: string } {
    return parseAttrValue(attrName, attrVal);
  }

  private static domName(attrName: string, attrVal: string): { name: string, value: string } {
    return parseAttrValue('name', attrVal);
  }

  private static uiwPopover(attrName: string, attrVal: string): { name: string, value: string } {
    console.warn('uiwPopover');
    return {
      name: 'tooltip',
      value: attrVal,
    };
  }

  private static className(attrName: string, attrVal: string): { name: string, value: string } {
    if (attrVal.indexOf('{{') >= 0) {
      throw new Error('class属性不支持插值写法');
      // console.warn('class属性不支持插值写法');
    }
    return {
      name: 'class',
      value: attrVal,
    };
  }

  private static style(attrName: string, attrVal: string): { name: string, value: string } {
    if (attrVal.indexOf('{{') >= 0) {
      // 如果style要支持插值语法的话，则需要考虑和ng-style的合并问题
      throw new Error('style属性不支持插值写法');
      // console.warn('style属性不支持插值写法');
    }
    return {
      name: 'style',
      value: attrVal,
    };
  }

  private static ngClass(attrName: string, attrVal: string): { name: string, value: string } {
    return {
      name: ':class',
      value: attrVal,
    };
  }

  private static ngStyle(attrName: string, attrVal: string): { name: string, value: string } {
    return {
      name: ':style',
      value: attrVal,
    };
  }

  private static ngHref(attrName: string, attrVal: string): { name: string, value: string } {
    return parseAttrValue('href', attrVal);
  }

  private static ngSrc(attrName: string, attrVal: string): { name: string, value: string } {
    return parseAttrValue('src', attrVal);
  }

  private static ngClick(attrName: string, attrValue: string) {
    return {
      name: '@click',
      value: attrValue,
    };
  }

  private static actionId(attrName: string, attrVal: string) {
    return {
      name: 'v-action-id',
      value: `'${attrVal}'`,
    };
  }

  private static ngIf(attrName: string, attrVal: string) {
    return {
      name: 'v-if',
      value: attrVal,
    };
  }

  private static ngDisabled(attrName: string, attrVal: string) {
    return {
      name: ':disabled',
      value: attrVal,
    };
  }

  private static ngShow(attrName: string, attrVal: string) {
    return {
      name: 'v-show',
      value: attrVal,
    };
  }

  private static ngclipboard(attrName: string, attrVal: string) {
    console.warn('ngclipboard');
    return {

    };
  }

  private static ngclipboardSuccess(attrName: string, attrVal: string) {
    console.warn('ngclipboardSuccess');
    return {

    };
  }

  private static dataClipboardText(attrName: string, attrVal: string) {
    console.warn('dataClipboardText');
    return {

    };
  }
}
