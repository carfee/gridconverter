import * as glob from 'glob';
import * as fse from 'fs-extra';
import * as htmlparser2 from 'htmlparser2';
import { resolve } from 'path';

import { excludeTplIds } from '.';
import { getAllNgTemplates } from './extractNgTpls';

export function extractText(tplId, ngTpl: string) {
  const texts = {};
  ngTpl = ngTpl.replace(/\{\{/g, '<![CDATA[{{').replace(/\}\}/g, '}}]]>');
  const parser = new htmlparser2.Parser({
    ontext(value: string) {
      if (value.trim()) {
        texts[tplId] = texts[tplId] || [];
        texts[tplId].push(value);
      }
    },
  }, {
    recognizeCDATA: true,
  });
  parser.write(ngTpl);
  parser.end();
  return texts;
}

if (resolve(__dirname, __filename) === process.argv[1]) {
  const dir = process.argv[2];
  if (dir) {
    const allTpls = getAllNgTemplates(dir);
    const allTexts = {};
    Object.keys(allTpls).forEach(htmlFilePath => {
      const tpls = allTpls[htmlFilePath];
      Object.keys(tpls).forEach(tplId => {
        const tpl = tpls[tplId];
        Object.assign(allTexts, extractText(tplId, tpl));
      });
    });

    console.log(JSON.stringify(allTexts, null, 2));
  }
}
