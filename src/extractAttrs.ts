import * as glob from 'glob';
import * as fse from 'fs-extra';
import * as htmlparser2 from 'htmlparser2';
import { resolve } from 'path';

import { excludeTplIds } from '.';
import { getAllNgTemplates } from './extractNgTpls';

export function extractAttrs(tplId, ngTpl: string) {
  const attrs = {};
  ngTpl = ngTpl.replace(/\{\{/g, '<![CDATA[{{').replace(/\}\}/g, '}}]]>');
  const parser = new htmlparser2.Parser({
    onattribute(name: string, value: string) {
      value = value.replace(/\<\!\[CDATA\[\{\{/g, '{{').replace(/\}\}\]\]\>/g, '}}');
      attrs[name] = attrs[name] || [];
      attrs[name].push(`||${value}||        ${tplId}`);
    },
  }, {
    recognizeCDATA: true,
  });
  parser.write(ngTpl);
  parser.end();
  return attrs;
}

if (resolve(__dirname, __filename) === process.argv[1]) {
  const dir = process.argv[2];
  if (dir) {
    const allTpls = getAllNgTemplates(dir);
    const allAttrs = {};
    Object.keys(allTpls).forEach(htmlFilePath => {
      const tpls = allTpls[htmlFilePath];
      Object.keys(tpls).forEach(tplId => {
        const tpl = tpls[tplId];
        const attrs = extractAttrs(tplId, tpl);
        Object.keys(attrs).forEach(attrName => {
          allAttrs[attrName] = allAttrs[attrName] || [];
          allAttrs[attrName].push(...attrs[attrName]);
        });
      });
    });

    console.log(JSON.stringify(allAttrs, null, 2));
    console.log(JSON.stringify(Object.keys(allAttrs), null, 2));
  }
}
