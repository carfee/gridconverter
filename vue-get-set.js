var base = { name: 'base', parent: null };
/* Vue reactive */
var rawVal = base.parent;
Object.defineProperty(base, 'parent', {
  get() { return rawVal; },
  set(newParent) { rawVal = newParent; }
});
/* end vue reaction */

/* 模拟ng scope */
var Child = function () { };
Child.prototype = base;
var child = new Child();
/*
其实只是想设置child的parent属性，确设置到base上去了
getter setter并非像原型链上的属性，读自原型链，写在自身上
*/
child.parent = base;
