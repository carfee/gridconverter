1. 将cellTemplate编译成vue组件(cell template vue component)
1. 将grid.svc中的cellTemplate重新手写成vue组件
1. 提供webpack loader对grid的cellTemplate进行编译
1. 提供cell组件用于提供渲染cell template vue component. `cell component`为`cell template vue component`提供上下文(grid, row, col)
1. 提供`vue-ui-grid`用于兼容ui-grid的options配置。